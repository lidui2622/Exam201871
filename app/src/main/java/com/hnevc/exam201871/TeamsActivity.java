package com.hnevc.exam201871;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.List;

public class TeamsActivity extends AppCompatActivity {

    ListView mLvTeams;
    MyTeamsAdapter adapter;

    //保存所有组的数据
    List<Team> teamList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams);

        initData();
        initView();

    }

    private void initData() {
        //获取raw文件的字符流
        InputStream in = getResources().openRawResource(R.raw.teams);
        Reader reader = new InputStreamReader(in);
        //定义gson转换类型
        Type listType = new TypeToken<List<Team>>(){}.getType();
        Gson gson = new Gson();
        //将json转换为teamList
        teamList = gson.fromJson(reader, listType);
    }

    private void initView() {
        mLvTeams = findViewById(R.id.lv_teams);
        adapter = new MyTeamsAdapter();
        mLvTeams.setAdapter(adapter);
    }

    class MyTeamsAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return teamList.size();
        }

        @Override
        public Team getItem(int position) {
            return teamList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = View.inflate(TeamsActivity.this,
                    R.layout.list_item,
                    null);

            TextView tvId;
            ImageView ivPhoto;
            TextView tvMatchs;
            TextView tvGoals;
            TextView tvScore;

            tvId = view.findViewById(R.id.tv_id);
            ivPhoto = view.findViewById(R.id.iv_pic);
            tvMatchs = view.findViewById(R.id.tv_matchs);
            tvGoals = view.findViewById(R.id.tv_goal);
            tvScore = view.findViewById(R.id.tv_score);

            final Team team = teamList.get(position);
            tvId.setText(team.getId());
            ivPhoto.setImageResource(getResourceByReflect(team.getPic()));
            tvMatchs.setText("比赛情况: 胜 " + team.getSheng() + " 平 " + team.getPing() + " 负 " + team.getFu());
            tvGoals.setText("进球情况: 进球 " + team.getWingoal() + " 失球 " + team.getLosegoal() + " 净胜球 " + team.getTotalgoal());
            tvScore.setText(team.getScore()+"");

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TeamsActivity.this, DetailActivity.class);
                    intent.putExtra("team",team);
                    //传递当前队伍对象到DetialActivity中

                    startActivity(intent);
                }
            });
            return view;
        }

        public int getResourceByReflect(String imageName) {
            Class drawable = R.drawable.class;
            Field field = null;
            int r_id;
            try {
                field = drawable.getField(imageName);
                r_id = field.getInt(field.getName());
            } catch (Exception e) {
                r_id = R.drawable.not_find;
                Log.e("ERROR", "PICTURE NOT　FOUND！");
            }
            return r_id;

        }


    }

}
