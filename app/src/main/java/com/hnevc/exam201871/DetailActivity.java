package com.hnevc.exam201871;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Field;

public class DetailActivity extends AppCompatActivity {

    TextView tvId;
    ImageView ivPhoto;
    TextView tvMatchs;
    TextView tvGoals;
    TextView tvScore;

    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        Intent intent = getIntent();

        team = (Team) intent.getSerializableExtra("team");
        if (team!=null){ //如果传递过来的队伍参数不为空，则更新界面
            initView();
        }
    }

    private void initView() {

        tvId = findViewById(R.id.tv_id);
        ivPhoto = findViewById(R.id.iv_pic);
        tvMatchs = findViewById(R.id.tv_matchs);
        tvGoals = findViewById(R.id.tv_goal);
        tvScore = findViewById(R.id.tv_score);

        tvId.setText(team.getId());
        ivPhoto.setImageResource(getResourceByReflect(team.getPic()));
        tvMatchs.setText("比赛情况: 胜 " + team.getSheng() + " 平 " + team.getPing() + " 负 " + team.getFu());
        tvGoals.setText("进球情况: 进球 " + team.getWingoal() + " 失球 " + team.getLosegoal() + " 净胜球 " + team.getTotalgoal());
        tvScore.setText("积分："+team.getScore()+"");


    }

    /**
     * 根据名字找到对应的图片资源id
     * @param imageName
     * @return
     */
    public int getResourceByReflect(String imageName) {
        Class drawable = R.drawable.class;
        Field field = null;
        int r_id;
        try {
            field = drawable.getField(imageName);
            r_id = field.getInt(field.getName());
        } catch (Exception e) {
            r_id = R.drawable.not_find;
            Log.e("ERROR", "PICTURE NOT　FOUND！");
        }
        return r_id;

    }
}
