package com.hnevc.exam201871;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    Button mBtnLogin;      //接收按钮控件
    EditText mEtUserName;  //接收用户名的控件
    EditText mEtPassword;  //接收密码的控件

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
        initEvent();
    }

    private void initEvent() {
        //登录按钮的点击事件
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = mEtUserName.getText().toString();
                String password = mEtPassword.getText().toString();

                if(userName.equals("admin") &&password.equals("123")){
                    //跳转到下一个页面
                    Intent intent = new Intent(LoginActivity.this, TeamsActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(LoginActivity.this, "用户名或密码错误！", Toast.LENGTH_SHORT).show();
                    //显示输出Toast信息

                }
            }
        });
    }

    private void initView() {
        //获取控件
        mBtnLogin = findViewById(R.id.btn_login);
        mEtUserName = findViewById(R.id.et_username);
        mEtPassword = findViewById(R.id.et_password);
    }
}