package com.hnevc.exam201871;


import java.io.Serializable;

/**
 * Created by Administrator on 2018/7/4.
 */

public class Team  implements Serializable{

    /**
     * id : A3
     * group : A
     * name : 埃及
     * sheng : 0
     * ping : 0
     * fu : 2
     * wingoal : 1
     * losegoal : 4
     * totalgoal : -3
     * score : 0
     * pic : aiji.png
     */

    private String id;
    private String group;
    private String name;
    private int sheng;
    private int ping;
    private String fu;
    private int wingoal;
    private int losegoal;
    private int totalgoal;
    private int score;
    private String pic;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSheng() {
        return sheng;
    }

    public void setSheng(int sheng) {
        this.sheng = sheng;
    }

    public int getPing() {
        return ping;
    }

    public void setPing(int ping) {
        this.ping = ping;
    }

    public String getFu() {
        return fu;
    }

    public void setFu(String fu) {
        this.fu = fu;
    }

    public int getWingoal() {
        return wingoal;
    }

    public void setWingoal(int wingoal) {
        this.wingoal = wingoal;
    }

    public int getLosegoal() {
        return losegoal;
    }

    public void setLosegoal(int losegoal) {
        this.losegoal = losegoal;
    }

    public int getTotalgoal() {
        return totalgoal;
    }

    public void setTotalgoal(int totalgoal) {
        this.totalgoal = totalgoal;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
